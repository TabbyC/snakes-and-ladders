using SnakesAndLadders;
using Xunit;

namespace SnakesAndLaddersTest
{
    public class EventTest
    {
        [Fact]
        public void TestCreateTrivialSnake()
        {
            var snake = new Event(Event.EventType.Snake, 2, 100);
            Assert.Equal(snake.Destination, 1);
        }

        [Fact]
        public void TestCreateMultipleSnakes()
        {
            for (int i = 1; i <= 10; i++) {
                var snake = new Event(Event.EventType.Snake, i*10, 100);
                Assert.True(snake.Destination < i*10);
                Assert.True(snake.Destination > 0);
            }
        }

        [Fact]
        public void TestCreateTrivialLadder()
        {
            var ladder = new Event(Event.EventType.Ladder, 1, 2);
            Assert.Equal(ladder.Destination, 2);
        }

        [Fact]
        public void TestCreateMultipleLadders()
        {
            for (int i = 1; i < 10; i++) {
                var ladder = new Event(Event.EventType.Ladder, i*10, 100);
                Assert.True(ladder.Destination >= i*10);
                Assert.True(ladder.Destination <= 100);
            }
        }
    }
}
