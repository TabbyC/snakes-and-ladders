using System;

namespace SnakesAndLadders
{
    /// <summary>
    /// Represents a snake or a ladder
    /// </summary>
    public class Event
    {
        private static readonly Random _random = new Random();

        public Event(EventType type, int startLocation, int boardSize) {
            if (type == EventType.Snake) {
                Destination = _random.Next(1, startLocation);
            } else {
                Destination = _random.Next(startLocation + 1, boardSize);
            }
            Type = type;
        }

        public EventType Type { get; }

        public int Destination { get; }

        public enum EventType {
            Snake,
            Ladder
        }
    }
}
