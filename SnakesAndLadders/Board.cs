using System;
using System.Collections.Generic;

namespace SnakesAndLadders
{
    public class Board
    {
        private readonly Dictionary<int, Event> _snakesAndLadders = new Dictionary<int, Event>();
        private readonly int _boardSize = 100; // 10x10

        public Board(int numSnakes, int numLadders) {
            var random = new Random();
            for (int i = 0; i < numSnakes; i++) {
                AddSnake(random);
            }
            for (int i = 0; i < numLadders; i++) {
                AddLadder(random);
            }
        }

        public void CheckIfPlayerOnSnakeOrLadder(Player player) {
            if (_snakesAndLadders.TryGetValue(player.Location, out Event boardEvent)) {
                if (boardEvent.Type == Event.EventType.Snake) {
                    Console.WriteLine($"Oh no! {player.Name} landed on a snake!");
                } else {
                    Console.WriteLine($"Yay! {player.Name} landed on a ladder!");
                }
                player.Location = boardEvent.Destination;
                Console.WriteLine($"{player.Name} is now on square {player.Location}");
            }
        }
        
        private void AddSnake(Random random) {
            AddSnakeOrLadder(random, Event.EventType.Snake);
        }

        private void AddLadder(Random random) {
            AddSnakeOrLadder(random, Event.EventType.Ladder);
        }

        private void AddSnakeOrLadder(Random random, Event.EventType type) {
            var startLocation = random.Next(1, 100);
            while (_snakesAndLadders.ContainsKey(startLocation)) {
                // Re-roll start point if another snake/ladder already starts there
                startLocation = random.Next(1, 100);
            }
            _snakesAndLadders.Add(startLocation, new Event(type, startLocation, _boardSize));
        }
    }
}
