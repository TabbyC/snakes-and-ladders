﻿using System;
using System.Linq;
using System.Text;

namespace SnakesAndLadders
{
    class Program
    {
        private static readonly Random _die = new Random();
        private static Player[] _players;
        private static Board _board;

        static void Main(string[] args)
        {
            Console.WriteLine("Snakes and Ladders!");
            Console.WriteLine("");
            Console.WriteLine("Enter number of players: (2-4)");
            var numPlayers = 0;
            while (numPlayers == 0) {
                try {
                    var value = Int32.Parse(Console.ReadLine());
                    if (value < 2 || value > 4) {
                        Console.WriteLine("Invalid number of players. Please enter a number between 2 and 4");
                        continue;
                    }
                    numPlayers = value;
                } catch {
                    Console.WriteLine("Error getting number of players. Please try again");
                }
            }

            _players = new Player[numPlayers];
            for (var i = 0; i < numPlayers; i++) {
                Console.WriteLine($"Please enter name for player {i+1}:");
                _players[i] = new Player(Console.ReadLine());
                Console.WriteLine($"Welcome {_players[i].Name}");
                Console.WriteLine("");
            }

            _board = new Board(numSnakes: 10, numLadders: 9);

            var winner = PlayGame();
            Console.WriteLine($"{winner.Name} has won! Congratulations!");
            Console.WriteLine("Thanks for playing!");
        }

        ///<summary>Plays through the game</summary>
        ///<returns>The winning player</returns>
        private static Player PlayGame() {
            while (true) {
                foreach (var player in _players)
                {
                    MovePlayer(player);
                    if (IsGameComplete(player)) {
                        return player;
                    }
                    Console.WriteLine("");
                }
                DisplayBoard();
            }
        }

        private static void MovePlayer(Player player) {
            var roll = _die.Next(1, 7);
            player.Location += roll;
            Console.WriteLine($"{player.Name} rolled a {roll} and moves to square {player.Location}");
            _board.CheckIfPlayerOnSnakeOrLadder(player);
        }

        private static bool IsGameComplete(Player player) {
            return player.Location >= 100;
        }

        private static void DisplayBoard() {
            StringBuilder sb = new StringBuilder("Current positions - ");
            _players
                .OrderByDescending(x => x.Location)
                .ToList()
                .ForEach(x => sb.Append($"{x.Name} {x.Location}, "));
            var positionsString = sb.ToString();
            Console.WriteLine(positionsString.Substring(0, positionsString.Length - 2));
            Console.WriteLine("");
        }
    }
}
