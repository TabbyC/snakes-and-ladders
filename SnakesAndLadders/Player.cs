using System;

namespace SnakesAndLadders
{
    public class Player
    {
        public Player(string name) {
            Name = name;
            Location = 1;
        }

        public string Name { get; }

        public int Location { get; set; }
    }
}
