Snakes and Ladders
==================

Snakes and Ladders is a board game involving two or more players rolling dice in order to move their tokens across a board. The board is made up of a collection of numbered squares and is adorned with 'snakes' and 'ladders', which link two squares on the board- snakes link the squares downwards whilst ladders link them going upwards. This means that landing at the bottom of a ladder moves you to the top of that ladder, whereas landing on the top of a snake moves you to the bottom of that snake. The objective of the game is to get your token to the final square before your opponents do.

TODO
----
* ~~Allow a player to roll a die and move through the board to the end~~
* ~~Have 2-4 players in the game~~
* ~~Declare first player to reach the end the winner and end the game~~
* ~~Add snakes to bring players back on the board~~
* ~~Add ladders to bring players forwards on the board~~
* Randomly choose order of play
* Have multiple sizes of board; small (8x8), medium (10x10), and large (12x12)
* Have variable difficulty; easy (more ladders, fewer snakes), normal, hard (more snakes, fewer ladders)
* Display the board visually